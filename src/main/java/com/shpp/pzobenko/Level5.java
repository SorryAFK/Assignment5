package com.shpp.pzobenko;

import com.shpp.pzobenko.constandprop.Prop;
import com.shpp.pzobenko.mongodbconnection.ClientMongoDB;
import com.shpp.pzobenko.mongodbconnection.Connection;
import com.shpp.pzobenko.mongodbconnection.InsertAndCount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;


/**
 * Create a tables, fuel tables with values and return the epicenter address with biggest num of selected category
 */
public class Level5 {

    private static final Logger log = LoggerFactory.getLogger(Level5.class);

    public static void main(String[] args) {
        if (args.length == 0 || args[0].isEmpty()) {
            log.error("Write arguments on console.", new NoSuchElementException());
        } else {
            log.info("Make connection to DB...");
            Prop prop = new Prop();
            ClientMongoDB clientMongoDB = new ClientMongoDB();
            Connection epicenter = new Connection(clientMongoDB.getMongoDB(), prop.getNameOfCollection());
            InsertAndCount insertAndCount = new InsertAndCount();
            insertAndCount.insertDataToDB(epicenter);
            log.info("The largest number of product category {} in the store at {}", args[0],
                    insertAndCount.getBiggestShopWithCategory(args[0], epicenter));
            clientMongoDB.close();
        }
    }
}