package com.shpp.pzobenko.validator.addresses;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidateDTOAddressOfGoods implements ConstraintValidator<ValidateDTOAddress, String> {
    @Override
    public void initialize(ValidateDTOAddress constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.length()>1;
    }
}