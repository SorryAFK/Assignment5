package com.shpp.pzobenko.mongodbconnection;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dtogeneratingandvalidation.ConvertToBSONAndWriteFindCommand;
import com.shpp.pzobenko.dtogeneratingandvalidation.DTOGenerator;
import org.apache.commons.lang3.time.StopWatch;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InsertAndCount {
    /**
     * Need to be empty.
     */
    public InsertAndCount() {
    }

    private static final Logger log = LoggerFactory.getLogger(InsertAndCount.class);
    public void insertDataToDB(Connection epicenterConnection) {
        DTOGenerator dtoGenerating = new DTOGenerator();
        log.info("Clearing DB");
        epicenterConnection.getEpicenterCollection().drop();
        StopWatch stopWatch = new StopWatch();
        log.info("Start inserting data to DB");
        stopWatch.start();
        while (dtoGenerating.getCount() != Constants.NUM_OF_DTO) {
            epicenterConnection.getEpicenterCollection().insertMany(dtoGenerating.generate());
        }
        stopWatch.stop();
        log.info("Time of inserting data to DB is {} seconds", stopWatch.getTime(TimeUnit.SECONDS));
    }

    public String getBiggestShopWithCategory(String arg ,Connection epicenter) {
        log.info("Finding biggest shop...");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        HashMap<String, Long> getResultOfFind = new HashMap<>();
        List<Document> documentsToFind = ConvertToBSONAndWriteFindCommand.createDocumentToTakeBigNum(arg);
        for (int i = 0; i < Constants.getAddresses().length; i++) {
            getResultOfFind.put(Constants.getAddresses()[i],
                    epicenter.getEpicenterCollection().countDocuments(documentsToFind.get(i)));
        }
        log.info("The big num of category {}", arg);
        String res = findBigNum(getResultOfFind);
        stopWatch.stop();
        log.info("Time of getting big store {}", stopWatch.getTime(TimeUnit.MILLISECONDS));
        return res;
    }

    private static String findBigNum(HashMap<String, Long> getResultOfSelect) {
        long findBiggest = 0;
        String result = null;
        Long getResultOfSelectGet;
        for (Map.Entry<String, Long> address : getResultOfSelect.entrySet()) {
            getResultOfSelectGet = address.getValue();
            if (findBiggest < getResultOfSelectGet) {
                result = address.getKey();
                findBiggest = getResultOfSelectGet;
            }
        }
        return result;
    }
}
