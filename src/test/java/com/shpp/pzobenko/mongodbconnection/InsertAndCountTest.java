package com.shpp.pzobenko.mongodbconnection;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InsertAndCountTest {
    @Test
    void getBiggestShopWithCategoryTest() {
        Connection epicenter = Mockito.mock(Connection.class);
        MongoCollection<Document> epicenterMock  = Mockito.mock(MongoCollection.class);
        InsertAndCount insertAndCount = new InsertAndCount();
        Mockito.when(epicenter.getEpicenterCollection()).thenReturn(epicenterMock);
        System.out.println( insertAndCount.getBiggestShopWithCategory("Tools", epicenter));
        Mockito.verify(epicenter,Mockito.times(10)).getEpicenterCollection();
    }
}