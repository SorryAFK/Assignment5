package com.shpp.pzobenko.constandprop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConstantsTest {

    @Test
    void getLettersTest() {
        int expected = 26;
        assertEquals(expected,Constants.getLetters().length);
    }

    @Test
    void getAddressesTest() {
        int expected = 10;
        assertEquals(expected,Constants.getAddresses().length);
    }

    @Test
    void getCategoriesTest() {
        int expected = 19;
        assertEquals(expected,Constants.getCategories().length);
    }
}