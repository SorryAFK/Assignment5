package com.shpp.pzobenko.dto;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataTransferObjectTest {

    private static DataTransferObject dto;

    @BeforeAll
    public static void setDataTransferObject() {
        dto = new DataTransferObject();
    }

    @Test
    void dtoSetAndGetIdOfProduct() {
        int expected = 1;
        dto.setIdOfProduct(1);
        assertEquals(expected, dto.idOfProduct());
    }

    @Test
    void dtoSetAndGetNameOfGood() {
        String expected = "forTestOnly";
        dto.setNameOfGood("forTestOnly");
        assertEquals(expected,dto.nameOfGood());
    }

    @Test
    void dtoSetAndGetCategoryName() {
        String expected = "testCategoryName";
        dto.setCategoryName("testCategoryName");
        assertEquals(expected,dto.categoryName());
    }


    @Test
    void dtoSetAndGetAddressOfShop() {
        String expected = "testAddressOfShop";
        dto.setAddressOfShop("testAddressOfShop");
        assertEquals(expected,dto.addressOfShop());
    }
}