package com.shpp.pzobenko.constandprop;

public class Constants {


    /**
     * Empty constructor.
     */
    private Constants() {
    }

    /**
     * Num of DTOs to send on tables products.
     */
    public static final int NUM_OF_DTO = 300000;
    /**
     * Length of goods name.
     */
    public static final int LENGTH_OF_GOODS_NAME = 20;
    /**
     * Number of goods per stream.
     */
    public static final int NUMBER_OF_GOODS_PER_STREAM = 10000;

    /**
     * name of field "id".
     */
    public static final String ID = "id";
    /**
     * name of field "category".
     */
    public static final String CATEGORY = "category";
    /**
     * name of field "nameOfProduct".
     */
    public static final String NAME_OF_PRODUCT = "nameOfProduct";
    /**
     * name of field "address".
     */
    public static final String ADDRESS = "address";


    /**
     * Category array size.
     */
    public static final int CATEGORY_SIZE = 19;
    /**
     * Address array size.
     */
    public static final int ADDRESS_SIZE = 10;
    /**
     * Array with addresses in Kiev.
     */
    private static final String[] ADDRESSES = new String[]{"Vulytsya Berkovetska 6B", "Nove Hwy 48",
            "Polyarna St 20D", "Stepana Bandery Ave 36А", "Bratyslavska St 11", "Kyivska St 253", "Kiltseva Rd 1B",
            "Petra Hryhorenka Ave 40", "Mahistralna St 2", "Vulytsya Horbatyuka 2"};
    /**
     * Some real categories from epicenter.
     */
    private static final String[] CATEGORIES = new String[]{"Repairs", "Floor covering", "Plumbing",
            "Garden and vegetable garden", "Pet products", "Illumination", "For home", "Tools", "Household appliances",
            "Electronics", "Children s goods", "Beauty and health", "Products", "Clothes, shoes, accessories",
            "Sports goods", "Tourism and the military", "Household chemicals", "For cars", "Stationery"};
    /**
     * Letter chars array which helps generates names for goods.
     */
    private static final char[] LETTERS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',};


    /**
     * get letters needs to secure array letters from changes.
     *
     * @return letters array.
     */
    public static char[] getLetters() {
        return LETTERS;
    }

    /**
     * get address needs to secure array address from changes.
     *
     * @return address array.
     */
    public static String[] getAddresses() {
        return ADDRESSES;
    }

    /**
     * get Category needs to secure array Category from changes.
     *
     * @return Category array.
     */
    public static String[] getCategories() {
        return CATEGORIES;
    }
}