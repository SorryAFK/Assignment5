package com.shpp.pzobenko.validator.category;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidateDTOCategoryOfGoods implements ConstraintValidator<ValidateDTOCategory, String> {
    @Override
    public void initialize(ValidateDTOCategory constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s.length() > 1;
    }
}