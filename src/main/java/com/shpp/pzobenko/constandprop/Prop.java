package com.shpp.pzobenko.constandprop;

import com.shpp.pzobenko.Level5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Loading properties from "prop.properties" file and creating getters for everyone
 */
public class Prop {
    private String url;
    private String nameOfDataBase;
    private String nameOfCollection;


    public Prop() {
        loadProp();
    }

    private static final Logger log = LoggerFactory.getLogger(Prop.class);

    private void loadProp() {
        log.info("Load properties...");
        Properties prop = new Properties();
        try {
            prop.load(Level5.class.getClassLoader().getResourceAsStream("prop.properties"));
        } catch (IOException e) {
            log.error("Can`t find properties file on resource");
        }
        url = prop.getProperty("dbUrl");
        nameOfDataBase = prop.getProperty("nameOfDataBase");
        nameOfCollection = prop.getProperty("nameOfCollection");
        log.info("All properties loads successful.");
    }

    public String getUrl() {
        return url;
    }

    public String getNameOfDataBase() {
        return nameOfDataBase;
    }

    public String getNameOfCollection() {
        return nameOfCollection;
    }

}