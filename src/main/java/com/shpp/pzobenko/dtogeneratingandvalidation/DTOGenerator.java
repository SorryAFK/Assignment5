package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dto.DataTransferObject;
import org.bson.Document;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class
DTOGenerator {
    private final AtomicInteger count;
    private final Validator validator;
    public DTOGenerator() {
        count = new AtomicInteger();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        factory.close();
    }

    public int getCount() {
        return count.get();
    }

    /**
     * Create stream which generating dto by random values and adding in the array
     */
    public List<Document> generate() {
        List<Document> listToAddObject = new ArrayList<>();
        Stream.generate(() -> generateDto(new DataTransferObject(), count.incrementAndGet())).
                filter(dto -> validator.validate(dto).isEmpty()).limit(Constants.NUMBER_OF_GOODS_PER_STREAM).
                forEach(dto -> listToAddObject.add(ConvertToBSONAndWriteFindCommand.createDocument(dto)));
        return listToAddObject;
    }

    /**
     * Create dto with random values
     *
     * @param dto needs to don`t create DataTransferObject each time
     * @return dto
     */
    private static DataTransferObject generateDto(DataTransferObject dto, int count) {
        dto.setCategoryName(Constants.getCategories()[ThreadLocalRandom.current().
                        nextInt(0, Constants.CATEGORY_SIZE)]).
                setAddressOfShop(Constants.getAddresses()[ThreadLocalRandom.current().
                        nextInt(0, Constants.ADDRESS_SIZE)]).
                setNameOfGood(generateName()).setIdOfProduct(count);
        return dto;
    }

    private static String generateName() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ThreadLocalRandom.current().nextInt(1, Constants.LENGTH_OF_GOODS_NAME); i++) {
            builder.append(Constants.getLetters()[ThreadLocalRandom.current().nextInt(Constants.getLetters().length)]);
        }
        return builder.toString();
    }
}