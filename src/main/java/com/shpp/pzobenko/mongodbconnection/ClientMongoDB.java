package com.shpp.pzobenko.mongodbconnection;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.shpp.pzobenko.constandprop.Prop;

public class ClientMongoDB {
    private final MongoClient mongoClient;
    private final MongoDatabase database;
    public ClientMongoDB() {
        Prop prop = new Prop();
        // Open a connection
        mongoClient = MongoClients.create(prop.getUrl());
        database = mongoClient.getDatabase(prop.getNameOfDataBase());
    }

    public MongoDatabase getMongoDB(){
        return database;
    }
    public void close(){
        mongoClient.close();
    }
}
