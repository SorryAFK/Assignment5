package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import org.bson.Document;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DTOGeneratingTest {

    private static DTOGenerator dtoGenerating;

    @BeforeAll
    public static void setDtoGenerating() {
        dtoGenerating = new DTOGenerator();
    }

    @Test
    void generateTest() {
        int expectedLengthOfList = Constants.NUMBER_OF_GOODS_PER_STREAM;
        List<Document> actual = dtoGenerating.generate();
        assertEquals(expectedLengthOfList, actual.size());
        Stream.iterate(0,i -> i+1).limit(actual.size()).forEach(i -> assertNotNull(actual.get(i)));
    }

    @Test
    @AfterEach
    void getCount() {
        assertEquals(Constants.NUMBER_OF_GOODS_PER_STREAM, dtoGenerating.getCount());
    }
}