package com.shpp.pzobenko.validator.nameofgoods;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidateDTONameOfGoods.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateDTOName {
    String message() default "The column 'nameOfGood' have length more than 40 or equals 0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
