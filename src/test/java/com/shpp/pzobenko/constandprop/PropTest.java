package com.shpp.pzobenko.constandprop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PropTest {

    private static Prop prop;
    @BeforeAll
    public static void setProp(){
        prop = new Prop();
    }

    @Test
    void getUrl() {
        assertNotNull(prop.getUrl());
    }

    @Test
    void getNameOfDataBase() {
        assertNotNull(prop.getNameOfDataBase());
    }

    @Test
    void getNameOfCollection() {
        assertNotNull(prop.getNameOfCollection());
    }
}