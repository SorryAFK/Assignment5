package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dto.DataTransferObject;
import org.bson.Document;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
class ConvertToBSONAndWriteFindCommandTest {

    @Test
    void createDocumentToTakeBigNum() {
        List<Document> expected = new ArrayList<>();
        for (String address : Constants.getAddresses()) {
            expected.add(new Document(Constants.CATEGORY, "test").append(Constants.ADDRESS, address));
        }
        List<Document> actual = ConvertToBSONAndWriteFindCommand.createDocumentToTakeBigNum("test");
        IntStream.range(0,expected.size()).forEach(everyOne -> assertEquals(expected.get(everyOne),actual.get(everyOne)));
    }

    @Test
    void createDocument() {
        DataTransferObject dto = new DataTransferObject();
        dto.setAddressOfShop("test1").setNameOfGood("test2").setCategoryName("test3").setIdOfProduct(1);
        Document expected = new Document(Constants.ID,dto.idOfProduct()).
                append(Constants.NAME_OF_PRODUCT,dto.nameOfGood()).append(Constants.CATEGORY,dto.categoryName()).
                append(Constants.ADDRESS,dto.addressOfShop());
        assertEquals(expected,ConvertToBSONAndWriteFindCommand.createDocument(dto));
    }
}