package com.shpp.pzobenko.dto;

import com.shpp.pzobenko.validator.addresses.ValidateDTOAddress;
import com.shpp.pzobenko.validator.category.ValidateDTOCategory;
import com.shpp.pzobenko.validator.nameofgoods.ValidateDTOName;

import javax.validation.constraints.Min;

public class DataTransferObject {

    @ValidateDTOName
    private String nameOfGood;
    @ValidateDTOCategory
    private String categoryName;
    @ValidateDTOAddress
    private String addressOfShop;
    @Min(1)
    private int idOfProduct;

    public int idOfProduct() {
        return idOfProduct;
    }

    public DataTransferObject setIdOfProduct(int idOfProduct) {
        this.idOfProduct = idOfProduct;
        return this;
    }

    public DataTransferObject() {
        //For initialization only
    }

    /**
     * Get name of good
     *
     * @return name of good
     */
    public String nameOfGood() {
        return nameOfGood;
    }

    /**
     * Set name of good
     *
     * @param nameOfGood name of good to set
     * @return this element to easier add new values by one line;
     */
    public DataTransferObject setNameOfGood(String nameOfGood) {
        this.nameOfGood = nameOfGood;
        return this;
    }

    /**
     * Get category name
     *
     * @return category name
     */
    public String categoryName() {
        return categoryName;
    }

    /**
     * Set category name
     *
     * @param categoryName category id to set
     * @return this element to easier add new values by one line;
     */
    public DataTransferObject setCategoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    /**
     * Get address of shop
     *
     * @return address of shop
     */
    public String addressOfShop() {
        return addressOfShop;
    }

    /**
     * Set address of shop
     *
     * @param addressOfShop address of shop set
     * @return this element to easier add new values by one line;
     */
    public DataTransferObject setAddressOfShop(String addressOfShop) {
        this.addressOfShop = addressOfShop;
        return this;
    }
}