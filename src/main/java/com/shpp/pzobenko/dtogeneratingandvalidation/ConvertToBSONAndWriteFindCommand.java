package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dto.DataTransferObject;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class ConvertToBSONAndWriteFindCommand {

    /**
     * Need to be empty.
     */
    private ConvertToBSONAndWriteFindCommand() {
    }


    /**
     * Creating select request for table products on id_epicenter column
     *
     * @param nameOfCategory needs to take only columns where have this category
     * @return List of requests
     */
    public static List<Document> createDocumentToTakeBigNum(String nameOfCategory) {
        List<Document> arrayOfCommands = new ArrayList<>();
        for (String address : Constants.getAddresses()) {
            arrayOfCommands.add(new Document(Constants.CATEGORY ,nameOfCategory).append(Constants.ADDRESS,address));
        }
        return arrayOfCommands;
    }


    public static Document createDocument(DataTransferObject d) {
        return new Document(Constants.ID,d.idOfProduct()).append(Constants.NAME_OF_PRODUCT,d.nameOfGood()).
                 append(Constants.CATEGORY,d.categoryName()).append(Constants.ADDRESS,d.addressOfShop());
    }
}