package com.shpp.pzobenko.mongodbconnection;


import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


public class Connection {

    private final MongoCollection<Document> epicenterCollection;

    public Connection(MongoDatabase database, String nameOfDB) {
        this.epicenterCollection = database.getCollection(nameOfDB);
    }

    public MongoCollection<Document> getEpicenterCollection() {
        return epicenterCollection;
    }
}